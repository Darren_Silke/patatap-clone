# Patatap Clone

Patatap Clone is a basic version of [Patatap](https://patatap.com). It was built using [Paper.js](http://paperjs.org) and [Howler.js](https://howlerjs.com).

A live version of Patatap Clone can be viewed [here](https://cloneofpatatap.netlify.app). To interact with it, press any key, A to Z, and turn up speakers.

## Running Locally

Clone the repository and open the 'index.html' file in your Web browser.

If you want to get rid of the Cross Origin errors in the console, you will need to use Live Server with [Visual Studio Code](https://code.visualstudio.com) (see [here](https://www.youtube.com/watch?v=fmQ_a1q3GOI) for instructions); or you will need to run a local HTTP server from the directory where Patatap Clone is, and load the 'index.html' file from there. The steps are as follows:

**Note:** If using Windows, see instructions [here](https://docs.google.com/document/d/1tq4F-E-dGB22O4qs7YsvpcdxXmwj9SJ0-GZVccyeaSU/edit).

1) Using the terminal, navigate to the directory where Patatap Clone is.

2) If you have Python v2.x installed, then run:

```sh
$ python -m SimpleHTTPServer
```

or for Python v3.x:

```sh
$ python -m http.server
```

3) Open up your Web browser, and navigate to *http://localhost:8000*

4) Select the 'index.html' file.
